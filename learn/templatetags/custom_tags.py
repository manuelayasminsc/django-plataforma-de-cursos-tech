from django import template

register = template.Library()

@register.filter
def get_first_letter(value):
    return value[0].upper()

