from django.core.paginator import Paginator


def calculate_total_courses(queryset, tag=None):
    '''
    Calcula o total de cursos da página.
    Caso haja uma tag na requisição, calcula o total de cursos a partir de uma determinada tag.

    :return: O número de cursos.
    '''
    if tag:
        return queryset.filter(tags__name=tag).count()
    return queryset.count() if queryset else 0

def get_paginator(queryset, request, per_page: int = 12):
    '''
    Cria uma paginação com 12 items por página.

    :return: Um paginator.
    '''
    paginator = Paginator(queryset, per_page)
    page_number = request.GET.get('page')
    return paginator.get_page(page_number)
