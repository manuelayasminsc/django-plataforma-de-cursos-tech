from taggit.models import Tag

from django.core.paginator import Paginator
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user

from cursos.models import Curso, CourseProgress
from cursos.utils import format_hours
from .utils import *



@login_required
def show_progress(request):
    user = get_user(request)
    courses_in_progress = CourseProgress.objects.filter(user=user, status='in_progress')[:3]
    completed_courses = CourseProgress.objects.filter(user=user, status='completed')[:3]

    context =  {
        'courses_in_progress': courses_in_progress,
        'completed_courses': completed_courses,
    }
    return render(request, 'progress.html', context)

@login_required
@csrf_exempt
def courses(request, tag=None, status=None):
    total_courses = 0 
    message = None

    user = request.user  
    title = request.GET.get('titulo')
    status = request.GET.get('status')

    queryset = Curso.objects.all()
    courses_by_progress = CourseProgress.objects.filter(user=user, status=status)

    if status:
        get_paginator(queryset=courses_by_progress, request=request)
        total_courses = calculate_total_courses(queryset=courses_by_progress)

    if tag:
        queryset = queryset.filter(tags__name=tag)
        total_courses = calculate_total_courses(queryset=queryset)

    elif title: 
        queryset = queryset.filter(titulo__icontains=title) 
        get_paginator(queryset=queryset, request=request)
        total_courses = calculate_total_courses(queryset=queryset)
 
        if not queryset.exists():
            message = f'Não há resultados para "{title}"'   
    
    tags = Tag.objects.all()

    context = {
        'courses': queryset,
        'courses_by_progress': courses_by_progress,
        'tags': tags,
        'total_courses': total_courses,
        'message': message,
        'user': user,
    }

    return render(request, 'courses.html', context)

@login_required
def course_info(request, course_pk):    
    course = Curso.objects.get(pk=course_pk)
   
    context = {
        'course': course,
        #'instructor': course.professor,
        'title': course.titulo,
        'description': course.descricao,
        'presentation': course.apresentacao,
        'level': course.nivel,
        'qt_modules': course.qtd_modulos,
        'course_duration': course.duracao_curso
    }
    return render(request, 'course_info.html', context)

