from django.urls import path
from . import views

app_name = 'learn'

urlpatterns = [ 
    path('', views.show_progress, name='progress'),
    path('courses/', views.courses, name='courses'),
    path('courses/<str:tag>/', views.courses, name='filtered_courses'),
    path('courses/<int:course_pk>/', views.course_info, name='course_info'),
]