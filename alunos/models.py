from django.db import models
from accounts.models import CustomUser

class Aluno(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.email

    class Meta:
        verbose_name = 'Aluno'
        verbose_name_plural = 'Alunos'