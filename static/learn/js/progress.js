
document.getElementById('completed-courses').style.display = 'none';
document.getElementById('courses-in-progress').style.display = 'flex';

function showInProgress() {
    document.getElementById('completed-courses').style.display = 'none';
    document.getElementById('courses-in-progress').style.display = 'flex';
    
    if (!document.getElementById('btn-in-progress').classList.contains('bg-transparent')) {
    document.getElementById('btn-in-progress').classList.add('bg-transparent', 'border-b-4', 'border-b-black', 'font-semibold');
    }

    document.getElementById('btn-completed').classList.remove('bg-transparent', 'border-b-4', 'border-b-black', 'font-semibold');

}

function showCompleted() {
    document.getElementById('courses-in-progress').style.display = 'none';
    document.getElementById('completed-courses').style.display = 'flex';
    document.getElementById('completed-courses').classList.add('justify-center', 'items-center'); 

    document.getElementById('btn-completed').classList.add('bg-transparent', 'border-b-4', 'border-b-black', 'font-semibold');
    document.getElementById('btn-in-progress').classList.remove('bg-transparent', 'border-b-4', 'border-b-black', 'font-semibold');
  
}
document.getElementById('btn-in-progress').addEventListener('click', showInProgress);
document.getElementById('btn-completed').addEventListener('click', showCompleted);

