var progress = document.getElementById("btn-in-progress");
progress.addEventListener("click", showProgress);

function showProgress() {
    document.getElementById('courses-in-progress').style.display = 'flex';
    document.getElementById('completed-courses').style.display = 'none'

}

var completed = document.getElementById('btn-completed');
completed.addEventListener('click', showCompleted);

function showCompleted() {
  document.getElementById('completed-courses').style.display = 'flex';
  document.getElementById('courses-in-progress').style.display = 'none';

}