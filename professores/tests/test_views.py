from model_mommy import mommy
from django.test import TestCase, Client
from django.urls import reverse
from accounts.models import CustomUser
from django.contrib.auth.models import Permission
from cursos.models import Curso
from django.core.paginator import Paginator
from professores.filters import CursoFilter


class TestInstrutorArea(TestCase):
    """Testa a área do instrutor"""
    def setUp(self):
        self.client = Client()

        self.my_user = CustomUser.objects.create_user(username='testing', email='myuser@gmail.com', password='123')
        self.user = mommy.make(CustomUser) 

        self.permission = Permission.objects.get(codename='acessar_area_do_professor')
        self.comum_permission = Permission.objects.get(codename='acessar_area_do_aluno')
  
    def test_user_have_instructor_permissions_to_access(self):
        """Deve retornar 200, o usuário pode acessar essa área"""
        self.user.user_permissions.add(self.permission)
        instructor_access = self.client.login(username='instructortest', password=self.user.password) 

        self.assertTrue(self.user.has_perm('accounts.acessar_area_do_professor'))
        response = self.client.get(reverse('professores:area_do_professor'))

        self.assertTrue(response.status_code, 200)

    def test_user_have_not_permission_to_access(self):
        """Deve retornar 403, o usuário não tem permissão para acessar a área do instrutor"""
        self.user.user_permissions.add(self.comum_permission)
        self.client.login(username='aluno', password=self.user.password)

        self.assertFalse(self.user.has_perm('accounts.acessar_area_do_professor'))
        response = self.client.get(reverse('professores:area_do_professor'))

        self.assertTrue(response.status_code, 403)
    
    def test_pagination(self):
        """Testa o comportamento da paginação na área do instrutor"""
        my_user_login = self.client.login(username='testing', email='myuser@gmail.com', password='123')
        self.assertTrue(my_user_login)

        my_user_perm = self.my_user.user_permissions.add(self.permission)
        self.assertTrue(self.my_user.has_perm('accounts.acessar_area_do_professor'))

        curso1 = mommy.make(Curso, 
                           titulo = 'Test 1',
                           descricao = 'Nenhuma decrição',
                           professor = self.my_user,
                           nivel = 'Iniciante',
                           status = 'Aprovado',
                           categoria = 'Outro',
                           prerequisitos = 'Nenhum pré-requisito',
                           slug = 'test-1',
                        )

        curso2 = mommy.make(Curso, 
                           titulo = 'Test 2',
                           descricao = 'Nenhuma decrição',
                           professor = self.my_user,
                           nivel = 'Iniciante',
                           status = 'Rascunho',
                           categoria = 'Outro',
                           prerequisitos = 'Nenhum pré-requisito',
                           slug = 'test-1',
                        )

        curso3 = mommy.make(Curso, 
                           titulo = 'Test 3',
                           descricao = 'Nenhuma decrição',
                           professor = self.my_user,
                           nivel = 'Iniciante',
                           status = 'Rascunho',
                           categoria = 'Outro',
                           prerequisitos = 'Nenhum pré-requisito',
                           slug = 'test-1',
                        )

        curso4 = mommy.make(Curso, 
                           titulo = 'Test 4',
                           descricao = 'Nenhuma decrição',
                           professor = self.my_user,
                           nivel = 'Iniciante',
                           status = 'Rascunho',
                           categoria = 'Outro',
                           prerequisitos = 'Nenhum pré-requisito',
                           slug = 'test-1',
                        )
                

        response = self.client.get(reverse('professores:area_do_professor'))

        self.assertTrue(response.status_code, 200)
        self.assertContains(response, curso1)

        response_context = len(response.context['cursos'])

        self.assertEqual(response_context, 4)

        objects = ['curso1', 'curso2', 'curso3', 'curso4']

        p = Paginator(objects, 4)
        page_1 = p.page(1)

        self.assertTrue(page_1)
        self.assertFalse(page_1.has_next())

    def test_status_filter(self):
        """Testa se o usuário consegue filtrar os cursos pelo status"""
        my_user_login = self.client.login(username='testing', email='myuser@gmail.com', password='123')
        self.assertTrue(my_user_login)

        my_user_perm = self.my_user.user_permissions.add(self.permission)
        self.assertTrue(self.my_user.has_perm('accounts.acessar_area_do_professor'))

        curso1 = mommy.make(Curso, 
                           titulo = 'Test 1',
                           descricao = 'Nenhuma decrição',
                           professor = self.my_user,
                           nivel = 'Iniciante',
                           status = 'Aprovado',
                           categoria = 'Outro',
                           prerequisitos = 'Nenhum pré-requisito',
                           slug = 'test-1',
                        )

        curso2 = mommy.make(Curso, 
                           titulo = 'Test 2',
                           descricao = 'Nenhuma decrição',
                           professor = self.my_user,
                           nivel = 'Iniciante',
                           status = 'Rascunho',
                           categoria = 'Outro',
                           prerequisitos = 'Nenhum pré-requisito',
                           slug = 'test-1',
                        )
        
        response = self.client.get(reverse('professores:area_do_professor') + '?status=Aprovado')
        self.assertContains(response, curso1)
        self.assertNotContains(response, curso2)

    def test_search_filter(self):
        """Testa se o usuário consegue realizar uma pesquisa"""
        my_user_login = self.client.login(username='testing', email='myuser@gmail.com', password='123')
        my_user_perm = self.my_user.user_permissions.add(self.permission)

        curso = mommy.make(Curso, 
                           titulo = 'Test Filter',
                           descricao = 'Nenhuma decrição',
                           professor = self.my_user,
                           nivel = 'Iniciante',
                           status = 'Aprovado',
                           categoria = 'Outro',
                           prerequisitos = 'Nenhum pré-requisito',
                           slug = 'test-filter',
                        )
        
        data = {'titulo__icontains': 'Test F'}
        filter = CursoFilter(data, queryset=Curso.objects.filter(professor=self.my_user))
        response = self.client.get(reverse('professores:area_do_professor'))

        filter_length = len(filter.qs)
        self.assertEqual(filter_length, 1)