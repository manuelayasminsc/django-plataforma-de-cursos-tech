from django import template

register = template.Library()

@register.filter()
def filter_status_message(status):
    if status == 'Pendente':
        return 'Não há cursos pendentes.'
    if status == 'Aprovado':
        return 'Não há cursos publicados.'
    else:
        return 'Não há cursos em rascunho.'

