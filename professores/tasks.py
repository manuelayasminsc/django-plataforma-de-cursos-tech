from celery import Celery, shared_task
from celery.utils.log import get_task_logger
from django.core.exceptions import ObjectDoesNotExist
from cursos.models import VideoAula


logger = get_task_logger(__name__)

app = Celery(
    broker='amqp://localhost'
    )

@shared_task(
        name='Processamento de video aula'
        ) 
def upload(aula_id, file_path):
    logger.info(f'Iniciando upload para aula {aula_id}')

    try:
        aula = VideoAula.objects.get(id=aula_id)
        aula.video = file_path
        aula.save()

        logger.info(f'Upload realizado com sucesso para {aula.titulo}')
        
        logger.info(f'Upload completo para {aula.titulo}')
    
    except ObjectDoesNotExist:
        logger.error(f'Aula {aula.titulo} não encontrada')
    
