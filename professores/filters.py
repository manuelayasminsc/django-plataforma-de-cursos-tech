import django_filters
from cursos.models import Curso

class CursoFilter(django_filters.FilterSet):
    titulo = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Curso
        fields = []
