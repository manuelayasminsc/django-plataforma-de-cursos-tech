from django.urls import path
from . import views
from . import views_update


app_name = "professores"

urlpatterns = [
    path('area-do-professor', views.area_do_professor, name='area_do_professor'),
    path('adicionar-curso/', views.adicionar_curso, name='adicionar_curso'),
    path('adicionar-modulos/', views.adicionar_modulos, name='adicionar_modulos'),
    path('adicionar-video/aula/', views.adicionar_video_aula, name='adicionar_video_aula'),
    
    path('alteracoes-curso-<int:id>/', views_update.alterar_curso, name='editar_curso'),

    path('alteracoes-modulos/', views.modulos, name='modulos'),
    path('alteracoes-modulos-editar-<int:modulo_id>/', views_update.editar_modulo, name='editar_modulo'),

    path('alteracoes-aulas/', views.aulas, name='aulas'),
    path('alteracoes-aulas-editar-<int:aula_id>/', views_update.editar_aula, name='editar_aula'),
    
    path('excluir-curso-<int:id>/', views.excluir_curso, name='excluir_curso'),
    ]

