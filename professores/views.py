import os

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages 
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user
from django.core.paginator import Paginator
from cursos.models import Curso, Modulo, VideoAula
from django.shortcuts import get_object_or_404
from .tasks import upload
from django.conf import settings
from .filters import CursoFilter
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import logout
from .status import HTTP_403_FORBIDDEN 


@login_required
def area_do_professor(request):
    instrutor = get_user(request)
    instrutor_cursos = instrutor.cursos.all().order_by('-created_at')
    status = request.GET.get('status')

    if not request.user.has_perm('accounts.acessar_area_do_professor'):
        return render(request, '403.html', status=HTTP_403_FORBIDDEN)
    
    if status:
        instrutor_cursos = instrutor_cursos.filter(status=status)
    
    cursos_filter = CursoFilter(request.GET, queryset=instrutor_cursos)
    
    if cursos_filter.is_valid():
        instrutor_cursos_filtrados = cursos_filter.qs
    else:
        instrutor_cursos_filtrados = instrutor_cursos

    paginator = Paginator(instrutor_cursos_filtrados, 4)
    numero_pagina = request.GET.get('page')
    cursos_pagina = paginator.get_page(numero_pagina)

    context = {
        'cursos_filter': cursos_filter.qs,
        'cursos': cursos_pagina,
        'instrutor_cursos': instrutor_cursos,
        'instrutor': instrutor,
        'status': status,
    }
    
    return render(request, 'area_do_professor.html', context)

@login_required
def modulos(request):
    id = request.session.get('id_curso')

    curso = get_object_or_404(Curso, id=id)
    modulos = curso.modulos.all()

    dados = {
        'curso': curso,
        'modulos': modulos,
    }

    return render(request, 'modulos.html', dados)

@login_required
def aulas(request):
    id = request.session.get('id_curso')
    
    curso = get_object_or_404(Curso, id=id)
    modulos = curso.modulos.all()
    aulas = VideoAula.objects.filter(modulo__in=modulos)

    dados = {
        'curso': curso,
        'modulos': modulos,
        'aulas': aulas
    }
    
    return render(request, 'aulas.html', dados)

@login_required
def adicionar_curso(request):
    if request.method == 'POST':
        dados = {
            'titulo': request.POST['titulo_curso'],
            'descricao': request.POST['descricao_curso'],
            'categoria': request.POST['categoria'],
            'nivel': request.POST.get('nivel'),
            'prerequisitos': request.POST.get('requisitos'),
            'professor': get_user(request)
        }

        request.session['dados_curso'] = dados

        try:
            curso = Curso(**dados)
            
            del request.session['dados_curso']
            
            curso.clean()
            curso.save()

            request.session['curso_id'] = curso.id
            return redirect('professores:adicionar_modulos')
        
        except ValidationError as e:
            messages.error(request, e.message)
            return redirect('professores:adicionar_curso')

    return render(request, 'novo_curso.html')

@login_required
def adicionar_modulos(request):
    curso_id = request.session.get('curso_id')
    curso = Curso.objects.get(id=curso_id)
    
    if request.method == 'POST':
        dados = {
            'titulo': request.POST['titulo_modulo'],
            'descricao': request.POST['descricao_modulo'],
            'curso': curso,
        }

        request.session['dados_modulos'] = dados

        try:
            modulo = Modulo(**dados)
            
            del request.session['dados_modulos']

            modulo.clean()
            modulo.save()

            request.session['modulo_id'] = modulo.id
            request.session['curso_id'] = modulo.curso_id

            return redirect('professores:adicionar_video_aula')

        except ValidationError as e:
            messages.error(request, e.message)

    return render(request, 'adicionar_modulos.html')

@login_required
def adicionar_video_aula(request):
    if request.method == 'POST':
        file = request.FILES.get('aula') 

        if file is not None:
            upload_dir = os.path.join(settings.MEDIA_ROOT, 'cursos', 'videos')
            destination = os.path.join(upload_dir, file.name)

            with open(destination, 'wb+') as destination_file:
                for chunk in file.chunks():
                    destination_file.write(chunk)

            file_path = os.path.join('cursos', 'videos', file.name)
            
            dados = {
                'titulo': request.POST['titulo_aula'],
                'descricao': request.POST['descricao_aula'],
            }
        
            request.session['dados_aulas'] = dados

            aula = VideoAula(**dados)
                
            del request.session['dados_aulas']

            request.session['video_aula_id'] = aula.id
            modulo_id = request.session['modulo_id']
            
            modulo = Modulo.objects.get(id=modulo_id)
            aula.modulo = modulo
            aula.save()

            upload(aula.id, file_path) 
                    
        else: 
            return render(request, 'adicionar_video_aula.html')

    if 'dados_modulos' in request.session:
        dados_modulos = request.session['dados_modulos']

    else:
        dados_modulos = {}

    modulos = Modulo.objects.filter(**dados_modulos)

    context = {
        'modulos': modulos,
    }

    return render(request, 'adicionar_video_aula.html', context)

@login_required
def alterar_curso(request, id):
    curso = get_object_or_404(Curso, id=id)
    
    request.session['id_curso'] = curso.id

    if request.method == 'POST':
        if 'nova_categoria' in request.POST:
            curso.categoria = request.POST.get('nova_categoria')
            
        curso.titulo =  request.POST.get('novo_titulo')
        curso.descricao = request.POST.get('nova_descricao')
        curso.nivel = request.POST.get('novo_nivel')
        curso.prerequisitos = request.POST.get('novos_requisitos')
     
        try:
            curso.save()

        except ValidationError as e:
            messages.error(request, e.message)

    dados = {'curso': curso}
    template = 'curso.html'

    return render(request, template, dados)

@login_required
def editar_modulo(request, modulo_id):
    modulo = get_object_or_404(Modulo, id=modulo_id)
    curso_id = modulo.curso.id

    if request.method == 'POST':
        modulo.titulo = request.POST.get('novo_titulo_modulo')
        modulo.descricao = request.POST.get('nova_descricao_modulo')

        modulo.save()
        
        return HttpResponseRedirect(reverse('professores:modulos'))
    
    dados = {'modulo': modulo}
    template = 'modulos.html'
    
    return render(request, template, dados)

@login_required
def editar_aula(request, aula_id):
    aula = get_object_or_404(VideoAula, id=aula_id)

    if request.method == 'POST':
        aula.titulo = request.POST.get('novo_titulo_aula')
        aula.descricao = request.POST.get('nova_descricao_aula')

        file = request.FILES.get('novo_video_aula')

        if file:
            upload_dir = os.path.join(settings.MEDIA_ROOT, 'cursos', 'videos')
            file_path = os.path.join(upload_dir, file.name)

            with open(file_path, 'wb+') as destino_file:
                for chunk in file.chunks():
                    destino_file.write(chunk)

            aula.video.name = os.path.join('cursos', 'videos', file.name)

        aula.save()

        return HttpResponseRedirect(reverse('professores:aulas'))

    dados = {'aula': aula}
    template = 'aulas.html'

    return render(request, template, dados)

@login_required
def excluir_curso(request, id):
    curso = get_object_or_404(Curso, id=id)
    curso.delete()

    return redirect('professores:area_do_professor')

@login_required
def realizar_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('logar'))