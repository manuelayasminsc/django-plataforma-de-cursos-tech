import os

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages 
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from cursos.models import Curso, Modulo, VideoAula
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.conf import settings
from .tasks import upload

@login_required
@permission_required('accounts.editar_conteudo', raise_exception=True)
def alterar_curso(request, id):
    curso = get_object_or_404(Curso, id=id)
    
    request.session['id_curso'] = curso.id

    if request.method == 'POST':
        if 'nova_categoria' in request.POST:
            curso.categoria = request.POST.get('nova_categoria')
            
        curso.titulo =  request.POST.get('novo_titulo')
        curso.duracao_curso = request.POST.get('nova_duracao')
        curso.descricao = request.POST.get('nova_descricao')
        curso.nivel = request.POST.get('novo_nivel')
        curso.prerequisitos = request.POST.get('novos_requisitos')
     
        try:
            curso.save()

        except ValidationError as e:
            messages.error(request, e.message)

    dados = {'curso': curso}
    template = 'curso.html'

    return render(request, template, dados)

@login_required
@permission_required('accounts.editar_curso', raise_exception=True)
def editar_modulo(request, modulo_id):
    modulo = get_object_or_404(Modulo, id=modulo_id)
    curso_id = modulo.curso.id

    if request.method == 'POST':
        modulo.titulo = request.POST.get('novo_titulo_modulo')
        modulo.descricao = request.POST.get('nova_descricao_modulo')

        modulo.save()
        
        return HttpResponseRedirect(reverse('professores:modulos'))
    
    dados = {'modulo': modulo}
    template = 'modulos.html'
    
    return render(request, template, dados)

@login_required
@permission_required('accounts.editar_conteudo', raise_exception=True)
def editar_aula(request, aula_id):
    aula = get_object_or_404(VideoAula, id=aula_id)

    if request.method == 'POST':
        aula.titulo = request.POST.get('novo_titulo_aula')
        aula.descricao = request.POST.get('nova_descricao_aula')

        file = request.FILES.get('novo_video_aula')

        if file:
            upload_dir = os.path.join(settings.MEDIA_ROOT, 'cursos', 'videos')
            file_path = os.path.join(upload_dir, file.name)

            with open(file_path, 'wb+') as destino_file:
                for chunk in file.chunks():
                    destino_file.write(chunk)

            aula.video.name = os.path.join('cursos', 'videos', file.name)

        aula.save()

        return HttpResponseRedirect(reverse('professores:aulas'))

    dados = {'aula': aula}
    template = 'aulas.html'

    return render(request, template, dados)