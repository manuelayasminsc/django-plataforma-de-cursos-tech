from django.urls import reverse_lazy
from django.shortcuts import redirect
from allauth.account.views import PasswordResetView
from django.contrib import messages


class CustomPasswordResetView(PasswordResetView):
    def form_valid(self, form):
        response = super().form_valid(form)
        
        if form.is_valid():
            messages.success(self.request, "Um e-mail com instruções sobre como mudar sua senha será enviado para sua conta em alguns instantes.")

        return response

def create_instructor_profile(request):
    data = request.POST

