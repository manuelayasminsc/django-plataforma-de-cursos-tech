from django.test import TestCase
from django.core.exceptions import ValidationError
from accounts.models import CustomUser


class TestCustomUser(TestCase):
    def test_create_user(self):
        email = 'test@example.com'
        password = 'password123'
        user = CustomUser.objects.create_user(email=email, password=password)
        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_create_super_user(self):
        email = 'meuemail@gmail.com'
        password = 'minhasenha'
        super_user =  CustomUser.objects.create_superuser(email=email, password=password)
        self.assertEqual(super_user.email, email)
        self.assertTrue(super_user.check_password(password))

    def test_email_validator(self):
        invalid_email = 'invalid-email' 
        user = CustomUser(email=invalid_email)
        with self.assertRaises(ValidationError):
            user.full_clean()
            user.save()
