#from model_mommy import mommy

from django.test import TestCase
from django.test.client import Client
from django.contrib.messages import get_messages
from django.contrib.auth.models import Permission
from django.urls import reverse
from accounts.models import CustomUser


class TestSignUp(TestCase):
    """Testa a view de cadastro"""
    def setUp(self):
        self.c = Client()
        self.comum_permission = Permission.objects.get(codename='acessar_area_do_aluno')

    def test_user_sign_up(self):
        """Testa o redirecionamento do usuário e as permissões concedidas durante o cadastro"""
        data = {
            'email': 'user@gmail.com', 
            'password': 'user'
            }

        response = self.c.post(reverse('cadastro'), data=data)
        self.assertRedirects(response, reverse('learn:progress'))

        user = CustomUser.objects.get(email='user@gmail.com')
        self.assertTrue(user.has_perm('accounts.acessar_area_do_aluno'))

    def test_error_messages(self):
        """Testa o levantamento das mensagens de erro diante dados inválidos do usuário"""
        data = {
            'email':'email',
            'password': 'password'
        }

        response = self.c.post(reverse('logar'), data=data)
        self.assertNotEqual(response.status_code, 302)

        messages = get_messages(response.wsgi_request())
        self.assertEqual(len(messages), 1)

