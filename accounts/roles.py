from rolepermissions.roles import AbstractUserRole

class Aluno(AbstractUserRole):
    available_permissions = {'acessar_area_do_aluno': True}

class Professor(AbstractUserRole):
    available_permissions = {'acessar_area_do_professor': True,
                             'acessar_area_do_aluno': True}
