from django.contrib import admin
from .models import CustomUser

@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('email','type_user')
    search_fields = ('email','type_user')
    readonly_fields = ('password',)
