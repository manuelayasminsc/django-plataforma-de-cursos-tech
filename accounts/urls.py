from django.urls import path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('instructor/profile', views.create_instructor_profile, name='instructor-profile'),
]