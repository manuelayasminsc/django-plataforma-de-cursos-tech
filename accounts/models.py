from django.db import models 
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator

from django.utils.translation import gettext_lazy as _


class PerfilInstrutor(models.Model):       
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sobre = models.CharField(max_length=100)
    especializacao = models.CharField(max_length=50)
    imagem = models.ImageField(blank=True, null=True, upload_to='accounts/imagens_instrutor')
    repositorio = models.URLField(validators=[URLValidator(message='insira uma url válida.')])
    linkedin = models.URLField(blank=True, null=True, validators=[URLValidator])

    class Meta:
        verbose_name = ('Perfil do instrutor')
        verbose_name_plural = ('Perfis de intrutores')

    def __str__(self):
        return self.user.username

    def clean(self):
        if not self.sobre:
            raise ValidationError(
                {"sobre": _("preencha esse campo.")}
            )
        
    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)
