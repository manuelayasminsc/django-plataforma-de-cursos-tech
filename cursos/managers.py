from django.db import models


class CourseProgressQuerySet(models.QuerySet):
    def in_progress(self):
        return self.filter(completed=False)

    def completed(self):
        return self.filter(completed=True)


class CourseProgressManager(models.Manager):
    def get_queryset(self):
        return CourseProgressQuerySet(self.model, using=self._db)

    def in_progress(self):
        return self.get_queryset().in_progress()

    def completed(self):
        return self.get_queryset().completed()