from moviepy.editor import VideoFileClip

from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver

from .models import VideoAula, Modulo, Curso


@receiver(post_save, sender=VideoAula)
def calcular_duracao_aula(sender, instance, **kwargs):
    """
    Sinal que captura o evento 'post_save' para calcular a duração da aula.
    
    :return: A duração da aula em segundos.
    """
    file_path = instance.video.path
    clip = VideoFileClip(file_path)

    video_duration = int(clip.duration)
    VideoAula.objects.filter(pk=instance.pk).update(duracao_aula=video_duration)
    
@receiver(pre_save, sender=VideoAula)
def verificar_modulo_anterior(sender, instance, **kwargs):
    """
    Sinal que captura o evento 'pre_save' de uma instância de VideoAula.
    
    Armazena o módulo anterior antes de qualquer alteração no campo 'modulo' da aula.
    O módulo anterior pode ser acessado posteriormente e o campo 'modulo' da instância já existente
    de VideoAula é atualizado conforme as mudanças.
    """
    if instance.pk:
        try:
            instance._previous_module = VideoAula.objects.get(pk=instance.pk).modulo
        except VideoAula.DoesNotExist:
            instance._previous_module = None

@receiver([post_save, post_delete], sender=VideoAula)
def calcular_duracao_modulo(sender, instance, **kwargs):
    """
    Sinal que captura os eventos 'post_save' e 'post_delete' para calcular a duração do módulo.
    
    :return: A duração do módulo em segundos.
    """
    try:
        previous_module = getattr(instance, '_previous_module', None)

        if instance.modulo is not None:
            aulas = VideoAula.objects.filter(modulo=instance.modulo)
            duracao_modulo = sum(aula.duracao_aula or 0 for aula in aulas)
            Modulo.objects.filter(pk=instance.modulo.pk).update(duracao_modulo=duracao_modulo)

            print(f'Duração módulo:{duracao_modulo}')

        if previous_module and previous_module != instance.modulo:
            aulas = VideoAula.objects.filter(modulo=previous_module)
            duracao_modulo_anterior = sum(aula.duracao_aula for aula in aulas)
            Modulo.objects.filter(pk=previous_module.pk).update(duracao_modulo=duracao_modulo_anterior)
    except Modulo.DoesNotExist:
        pass

@receiver([post_save, post_delete], sender=VideoAula)
def calcular_qtd_aulas_modulo(sender, instance, **kwargs):
    """
    Sinal que captura os eventos 'post_save' e 'post_delete' para calcular a quantidade de aulas de um módulo.

    :return: O total de aulas em um módulo.
    """
    try:
        previous_module = getattr(instance, '_previous_module', None)

        if previous_module and previous_module != instance.modulo:
            qtd_aulas_modulo_anterior = VideoAula.objects.filter(modulo=previous_module).count()
            Modulo.objects.filter(pk=previous_module.pk).update(qtd_aulas=qtd_aulas_modulo_anterior)

        modulo = instance.modulo
        qtd_aulas = modulo.aulas.count()
        Modulo.objects.filter(pk=modulo.pk).update(qtd_aulas=qtd_aulas)
    except Modulo.DoesNotExist:
        pass

@receiver([post_save, post_delete], sender=Modulo)
def calcular_qtd_modulos(sender, instance, **kwargs):
    """
    Sinal que captura os eventos 'post_save' e 'post_delete' para calcular a quantidade de módulos totais.

    :return: O total de módulos de um curso.
    """
    try:     
        curso = instance.curso
        qtd_modulos = Modulo.objects.filter(curso=curso).count()
        Curso.objects.filter(pk=curso.pk).update(qtd_modulos=qtd_modulos)
    except Curso.DoesNotExist:
        pass

@receiver([post_save, post_delete], sender=VideoAula)
def calcular_duracao_curso(sender, instance, **kwargs):
    """
    Sinal que captura os eventos 'post_save' e 'post_delete' para calcular a quantidade de módulos totais.

    :return: A duração do curso em segundos.
    """
    previous_module = getattr(instance, '_previous_module', None)

    if previous_module and previous_module != instance.modulo:
        aulas_anterior = VideoAula.objects.filter(modulo=previous_module)
        duracao_modulo_anterior = sum(aula.duracao_aula for aula in aulas_anterior)
        Modulo.objects.filter(pk=previous_module.pk).update(
            duracao_modulo=duracao_modulo_anterior
        )

    try:
        modulo = instance.modulo
        curso = modulo.curso
        modulos = curso.modulos.all()
        duracao_curso = sum(modulo.duracao_modulo or 0 for modulo in modulos)
        Curso.objects.filter(pk=curso.pk).update(duracao_curso=duracao_curso)

    except Modulo.DoesNotExist:
        pass