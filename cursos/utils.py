from datetime import timedelta


def format_duration(seconds):
    """
    Formata a duração no formato HH:MM:SS

    :param seconds: A duração em segundos
    :returns: Uma string da duração formatada 
    """
    td = timedelta(seconds=seconds)

    hours, remainder = divmod(td.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    if hours == 0:
        return "{:02}:{:02}".format(int(minutes), int(seconds))

    return "{:02}:{:02}:{:02}".format(int(hours), int(minutes), int(seconds))

def format_hours(seconds):
    """
    Formata apenas a hora

    :param seconds: A duração em segundos
    :returns: Uma string somente da hora formatada sem exibir os minutos e segundos
    """
    td = timedelta(seconds=seconds)

    hours, remainder = divmod(td.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    if hours < 10:
        return "{:01}".format(int(hours))
    return format_duration(seconds)
    