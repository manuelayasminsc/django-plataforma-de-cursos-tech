from django.contrib import admin
from .models import Curso, VideoAula

@admin.register(Curso)
class CustomCurso(admin.ModelAdmin):
    list_display = ('nome','professor')
    search_fields = ('nome','categoria', 'professor')

@admin.register(VideoAula)
class CustomVideoAula(admin.ModelAdmin):
    list_display = ('titulo',)
    search_fields = ('titulo', 'curso')
