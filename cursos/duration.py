from datetime import timedelta


def format_duration(seconds):
    td = timedelta(seconds=seconds)

    hours, remainder = divmod(td.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)

    if hours == 0:
        return "{:02}:{:02}".format(int(minutes), int(seconds))
    else:
        return "{:02}:{:02}:{:02}".format(int(hours), int(minutes), int(seconds))
