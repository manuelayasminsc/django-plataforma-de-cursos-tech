from taggit.managers import TaggableManager

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from accounts.models import InstructorProfile


NIVEl = (
    ('Iniciante', 'Iniciante'),
    ('Intermediário', 'Intermediário'),
    ('Avançado', 'Avançado'),
)

STATUS = (
    ('Pendente', 'Pendente'),
    ('Aprovado', 'Aprovado'),
    ('Rejeitado', 'Rejeitado'),
    ('Rascunho', 'Rascunho'),
)

CATEGORIA = (
    ('Ciência De Dados', 'Ciência De Dados'),
    ('Analise De Dados', 'Analise De Dados'),
    ('Inteligência Artificial', 'Inteligência Artificial'),
    ('Desenvolvimento Web Backend', 'Desenvolvimento Web Backend'),
    ('Desenvolvimento Web Frontend', 'Desenvolvimento Web Frontend'),
    ('Outro', 'Outro'),
)


class Curso(models.Model):        
    titulo = models.CharField(max_length=50, blank=True, null=True, verbose_name='Título do curso')
    descricao = models.TextField(blank=True, null=True, verbose_name='Descrição')
    apresentacao = models.CharField(max_length=140, blank=True, null=True)
    professor = models.ForeignKey(InstructorProfile, on_delete=models.CASCADE, related_name='cursos')
    nivel = models.CharField(max_length=14, choices=NIVEl, verbose_name='Nível', default='Iniciante')
    status = models.CharField(max_length=10, choices=STATUS, default='Rascunho')
    categoria = models.CharField(max_length=30, choices=CATEGORIA, verbose_name='Categoria', null=True, blank=True)
    slug = models.SlugField(max_length=500, default='')
    qtd_modulos = models.IntegerField(null=True, blank=True, default=0, verbose_name='Quantidade de módulos')
    duracao_curso = models.IntegerField(blank=True, null=True, default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    tags = TaggableManager()
    

    class Meta:
        ordering = ['-created_at']

        verbose_name = "Curso"
        verbose_name_plural = "Cursos" 

    def __str__(self):
        return self.titulo

    def clean(self):
        if not self.pk and not all([self.titulo, 
                                    self.descricao,
                                    self.categoria, 
                                    self.nivel]):
            raise ValidationError('preencha todos os campos para prosseguir.')
        

    def validate_fields_on_update(self):
        if self.pk and not any([self.titulo, 
                                self.descricao, 
                                self.categoria,
                                self.nivel]):
            raise ValidationError('as alterações não podem ser salvas.')
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.titulo)
        self.clean()
        self.validate_fields_on_update()
        super().save(*args, **kwargs)

class PreRequisito(models.Model):
    curso = models.ForeignKey(Curso, related_name='requisitos', on_delete=models.CASCADE)
    requisito = models.CharField(max_length=40)

    class Meta:
        verbose_name =  'Pré-Requisito'
        verbose_name_plural = 'Pré-Requisitos'
    
    def __str__(self):
        return self.curso.titulo

class Modulo(models.Model):
    curso = models.ForeignKey(Curso, blank=True, null=True, on_delete=models.CASCADE, related_name='modulos')
    titulo = models.CharField(max_length=50, verbose_name='Título do módulo')
    descricao = models.TextField(verbose_name='Descrição do módulo', null=True)
    qtd_aulas = models.IntegerField(verbose_name='Quantidade de aulas', default='0')
    duracao_modulo = models.IntegerField(blank=True, null=True, default=0)
    slug = models.SlugField(null=True, blank=True, max_length=500)

    class Meta:
        verbose_name = 'Módulo'
        verbose_name_plural = 'Módulos'

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.titulo)
        super(Modulo, self).save(*args, **kwargs)

class VideoAula(models.Model):
    modulo = models.ForeignKey(Modulo, blank=True, null=True, on_delete=models.CASCADE, related_name='aulas')
    titulo = models.CharField(max_length=50, verbose_name='Título da video aula')
    descricao =  models.TextField(verbose_name='Descrição')
    video = models.FileField(upload_to='cursos/videos/', verbose_name='Vídeo aula')
    duracao_aula = models.IntegerField(blank=True, null=True, default=0)
    processed = models.BooleanField(default=False)
    slug = models.SlugField(null=True, blank=True, max_length=500)
   
    def __str__(self):
        return self.titulo

class Progress(models.Model):
    STATUS = (
        ('in_progress', 'in_progress'),
        ('completed', 'completed')
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE) 
    status = models.CharField(max_length=11, choices=STATUS , default='in_progress')

    class Meta:
        abstract = True

class CourseProgress(Progress):
    course = models.ForeignKey(Curso, on_delete=models.CASCADE, related_name='course_progress')

    def __str__(self):
        return self.course.titulo
    
    class Meta:
        verbose_name = 'Progresso do curso'
        verbose_name_plural = 'Progresso dos cursos'

class VideoProgress(Progress):
    video = models.ForeignKey(VideoAula, on_delete=models.CASCADE, related_name='video_progress')

    def __str__(self):
        return self.video.titulo
    
    class Meta:
        verbose_name = 'Progresso da aula'
        verbose_name_plural = 'Progresso das aulas'