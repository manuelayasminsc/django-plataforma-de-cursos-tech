from django.shortcuts import render
from django.contrib.auth.decorators import login_required


'''
- A página do curso em si.
'''

'''
@login_required
def get_user_progress(request):
    # Verificar o progresso do usuário
    # Se ele clicou no botão para continuar significa que completou mais uma aula
    # Se nem todas foram vistas ainda o status do progresso é status=in_progress
    # Se todas as aulas foram vistas então o progresso é completado (status=completed)
    # Essa informação será retornada pro endpoint learn/progress

    >> Outras formas de fazer e mais avançadas
    
    1 - Verificar progresso da aula através do tempo de video assistido verificando se o usuário passou o vídeo pra frente
    ...

'''