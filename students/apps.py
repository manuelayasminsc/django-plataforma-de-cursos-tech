from django.apps import AppConfig


class AreaDoAlunoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'area_do_aluno'
