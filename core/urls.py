from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from allauth.account import views
from accounts.views import CustomPasswordResetView

urlpatterns = [
    path('admin/', admin.site.urls),

    path('accounts/', include('allauth.urls')), 
    path("password/reset/", CustomPasswordResetView.as_view(), name="account_reset_password"),

    path("accounts/login/", views.login, name="account_reset_password_done"),

    path('users/', include('accounts.urls')),
    path('learn/', include('learn.urls')),
    path('', include('professores.urls', namespace="professores")),
    path('', include('cursos.urls')),    

] 

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root= settings.MEDIA_ROOT
                          )

